I wrote a small Hello World sample with Agatha to demonstrate how easy it is to use in a new project. 

Well, it's not really a small example since it's probably the most over-engineered Hello World app ever. 
Doesn't matter though, the objective is to demonstrate how you can get started with Agatha and it shows that pretty well.