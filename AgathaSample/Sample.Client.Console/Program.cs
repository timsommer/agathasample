﻿using System.ComponentModel;
using Agatha.Common;
using Agatha.Common.Caching;
using Agatha.Common.InversionOfControl;
using Agatha.ServiceLayer;
using Agatha.ServiceLayer.Conventions;
using Sample.Common;
using System;
using Sample.ServiceLayer.Handlers.Handlers;

namespace Sample.Client.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            InitializeAgatha();

            System.Console.WriteLine("This is a sample project that executes 2 service calls using the Agatha - Castle Windsor Framework.");
            System.Console.WriteLine("Press any key to execute the HelloWorld sample.");
            System.Console.ReadLine();
            ExecuteHelloWorld();
            System.Console.WriteLine();
            System.Console.WriteLine("Press any key to continue.");
            System.Console.ReadLine();
            System.Console.WriteLine("Executing Calulculation sample.");
            System.Console.WriteLine("Give a random number.");
            var _memberLeft = System.Console.ReadLine();
            System.Console.WriteLine("Give another random number.");
            var _memberRight = System.Console.ReadLine();
            System.Console.WriteLine("Invoking calculation service..");

            if (_memberLeft != null)
                if (_memberRight != null)
                    ExecuteCalculations(int.Parse(_memberLeft), int.Parse(_memberRight));

            System.Console.ReadLine();

        }


        /// <summary>
        /// Execute the HelloWorld sample
        /// </summary>
        private static void ExecuteHelloWorld()
        {
            //Resolve the requestdispatcher. This will  put the message through to the WCF service 

            var _requestDispatcher = IoC.Container.Resolve<IRequestDispatcher>();
            //var _requestDispatcher = IoC.Container.Resolve<IRequestDispatcher>();
            //Get the reponse when sending out a new HelloWorldRequest
            var _response = _requestDispatcher.Get<HelloWorldResponse>(new HelloWorldRequest());
            //Write it to the console
            System.Console.WriteLine(_response.Message);
        }

        /// <summary>
        /// Exactly the same but with extra properties on the request object.
        /// </summary>
        /// <param name="leftmember"></param>
        /// <param name="rightmember"></param>
        private static void ExecuteCalculations(int leftmember, int rightmember)
        {
            var _requestDispatcher = IoC.Container.Resolve<IRequestDispatcher>();
            var _response = _requestDispatcher.Get<CalculationResponse>(new CalculationRequest()
                {
                    LeftMember = leftmember,
                    RightMember = rightmember
                });
            System.Console.WriteLine(_response.Message);
        }

        /// <summary>
        /// Initialize Agatha for this client  
        /// </summary>
        private static void InitializeAgatha()
        {
            //new ServiceLayerAndClientConfiguration(typeof (HelloWorldHandler).Assembly,
            //    typeof(HelloWorldRequest).Assembly, new Agatha.Castle.Container())
            //    .Initialize();


            //new ServiceLayerAndClientConfiguration(typeof(HelloWorldHandler).Assembly,
            //                                       typeof(HelloWorldRequest).Assembly,
            //                                       new Agatha.Castle.Container())

            //                            .Use<RequestHandlerBasedConventions>()
            //                            .Initialize();

           

            new ServiceLayerAndClientConfiguration(
                typeof(HelloWorldHandler).Assembly, 
                typeof(HelloWorldRequest).Assembly, 
                new Agatha.Castle.Container()).Initialize();
            
            //new ClientConfiguration(typeof(HelloWorldRequest).Assembly, typeof(Agatha.Castle.Container)).Initialize();
        }

    }
}