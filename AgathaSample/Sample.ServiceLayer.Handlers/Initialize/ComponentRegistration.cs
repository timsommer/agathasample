﻿using System.Reflection;
using Agatha.ServiceLayer;
using Sample.Common;

namespace Sample.ServiceLayer.Handlers.Initialize
{
    /// <summary>
    /// Handles the Agatha initialization. Should be called by global.asax file of the ServiceLayer.Host
    /// </summary>
    public static class ComponentRegistration
    {
        //public static void Register()
        //{
        //    /* The ServiceLayerConfiguration's constructor requires 3 parameters. 
        //     * The first is the assembly that contains the Request Handlers (meaning the current in this project).
        //     * The second the one that contains your Request and Response types. 
        //     * The third parameter is either a reference to a Type which implement's Agatha's IContainer interface, 
        //     * or an instance of IContainer if you want Agatha to reuse an existing IOC container instance (like, the one the rest of your application is using).
        //     * If you pass in a reference to a type which implements IContainer instead of an actual instance, 
        //     * Agatha will create a new container and use that instead.              * 
        //     * All of the Request Handlers and Request and Response types found in the passed in assemblies will all be registered within the container automatically so you don't have to worry about that at all.
        //     */
            
        //    new ServiceLayerConfiguration(Assembly.GetExecutingAssembly(), typeof(HelloWorldRequest).Assembly,
        //    typeof(Agatha.Castle.Container)).Initialize();
        //}
    }
}