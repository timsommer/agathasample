﻿using Agatha.Common;
using Agatha.ServiceLayer;
using Sample.Common;

namespace Sample.ServiceLayer.Handlers.Handlers
{
    /// <summary>
    /// The http handler. When a client sends a 'HelloWorldRequest', this handler will be invoked
    /// </summary>
    public class HelloWorldHandler : RequestHandler<HelloWorldRequest, HelloWorldResponse>
    {
        /// <summary>
        /// The method that will be invoked
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public override Response Handle(HelloWorldRequest request)
        {
            var _response = CreateTypedResponse();
            _response.Message = "Hello World!";
            return _response;
        }
    }
}



