﻿using Agatha.Common;
using Agatha.ServiceLayer;
using Sample.Common;

namespace Sample.ServiceLayer.Handlers.Handlers
{
    public class CalculationHandler : RequestHandler<CalculationRequest, CalculationResponse>
    {
        public override Response Handle(CalculationRequest request)
        {
            var _response = CreateTypedResponse();
            _response.Message = request.LeftMember + " x " + request.RightMember + " = " + request.LeftMember*request.RightMember;
            return _response;
        }
    }
}