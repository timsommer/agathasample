﻿using Agatha.Common;

namespace Sample.Common
{
    /// <summary>
    /// the response object sent back to the client
    /// </summary>
    public class HelloWorldResponse : Response
    {
        public string Message { get; set; }
    }
}
