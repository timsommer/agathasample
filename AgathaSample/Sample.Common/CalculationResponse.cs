﻿using Agatha.Common;

namespace Sample.Common
{
    public class CalculationResponse : Response
    {
        public string Message { get; set; }
    }
}