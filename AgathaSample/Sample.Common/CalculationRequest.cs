﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Agatha.Common;

namespace Sample.Common
{
    public class CalculationRequest: Request
    {
        public int LeftMember { get; set; }
        public int RightMember { get; set; }
    }
}
