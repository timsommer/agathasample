﻿using Agatha.Common;

namespace Sample.Common
{
    /// <summary>
    /// The request object sent to the service
    /// </summary>
    public class HelloWorldRequest : Request { }
}